package com.google.ar.sceneform.samples.augmentedimage;

public class Rewards {

    private Integer rewardId;
    private String rewardCode;
    private String validUpTo;
    private String rewardDesc ;
private boolean isSeen;


    public void setSeen(boolean seen) {
        isSeen = seen;
    }

    public boolean isSeen() {
        return isSeen;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    public void setRewardId(Integer rewardId) {
        this.rewardId = rewardId;
    }

    public void setValidUpTo(String validUpTo) {
        this.validUpTo = validUpTo;
    }

    public void setRewardDesc(String rewardDesc) {
        this.rewardDesc = rewardDesc;
    }

    public String getRewardDesc() {
        return rewardDesc;
    }

    public Integer getRewardId() {
        return rewardId;
    }

    public String getRewardCode() {
        return rewardCode;
    }
    public Rewards(){

    }
    public Rewards(Integer rewardId, String rewardCode, String validUpTo,String rewardDesc,boolean isSeen) {
        this.rewardId = rewardId;
        this.rewardCode = rewardCode;
        this.validUpTo = validUpTo;
        this.rewardDesc=rewardDesc;
        this.isSeen=isSeen;

    }

    public String getValidUpTo() {
        return validUpTo;
    }
}
