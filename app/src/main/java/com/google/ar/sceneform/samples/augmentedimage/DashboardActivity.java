package com.google.ar.sceneform.samples.augmentedimage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;


public class DashboardActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
        loadRewardStore();

        Button showScoreBtn = (Button) findViewById(R.id.shwScore);
        Button rewardsBtn = (Button) findViewById(R.id.rewards);
        Button burnBtn=(Button)findViewById(R.id.scan);

        burnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                List<Rewards> userRewards=AppConstants.userRewardsStore.get(123);
                Set<Integer> rewardsAllocated=userRewards.stream().map(i->i.getRewardId()).collect(Collectors.toSet());
               while (true) {
                   int rndm = getRandomNum();
                   if (rewardsAllocated.add(rndm)) {
                       AppConstants.userRewardsStore.get(123).add(AppConstants.rewardsStore.get(rndm));
                       break;
                   }
               }
            }
        });

        showScoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppConstants.score++;
                Intent i = new Intent(DashboardActivity.this, ScoreActivity.class);
                startActivity(i);
            }
        });

        rewardsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppConstants.score++;
                Intent i = new Intent(DashboardActivity.this, CardListActivity.class);
                startActivity(i);
            }
        });
    }

    private int getRandomNum(){
        Random random = new Random();
        int rand = 0;
        while (true){
            rand = random.nextInt(11);
            if(rand !=0) break;
        }
        return  rand;
    }
    private  void loadRewardStore(){
        AppConstants.userRewardsStore.clear();
        AppConstants.userRewardsStore.put(123,new ArrayList<Rewards>());
        AppConstants.rewardsStore.clear();
        List<String> rewardsDescList=new ArrayList<>();
        rewardsDescList.add("Free MC D burger");
        rewardsDescList.add("Free Burgerking whooper");
        rewardsDescList.add("50% discount on apple products");
        rewardsDescList.add("Free Movie ticket");
        rewardsDescList.add("20% cashback on next transaction");
        rewardsDescList.add("30% discount in PVR");
        rewardsDescList.add("3 days 2 nights singapore trip");
        rewardsDescList.add("500rs Puma coupon");
        rewardsDescList.add("1000rs Addidas coupon");
        rewardsDescList.add("300rs Ola money");
        rewardsDescList.add("Free 1 uber ride");

     for(int i=1;i<=10;i++){
         Rewards rewards=new Rewards( i, UUID.randomUUID().toString().split("-")[0],"15/03/2020",rewardsDescList.get(i),false);
         AppConstants.rewardsStore.put(i,rewards);
     }
    }


}