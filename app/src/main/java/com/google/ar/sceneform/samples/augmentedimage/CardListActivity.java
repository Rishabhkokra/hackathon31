package com.google.ar.sceneform.samples.augmentedimage;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class CardListActivity extends Activity {

    private static final String TAG = "CardListActivity";
    private CardArrayAdapter cardArrayAdapter;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);
        listView = (ListView) findViewById(R.id.card_listView);

        listView.addHeaderView(new View(this));
        listView.addFooterView(new View(this));

        cardArrayAdapter = new CardArrayAdapter(getApplicationContext(), R.layout.list_item_card);

        for (int i = 0; i <  AppConstants.userRewardsStore.get(123).size(); i++) {
            Rewards rewards= AppConstants.userRewardsStore.get(123).get(i);
            Card card = new Card("Code: "+rewards.getRewardCode() +" |  Description: "+rewards.getRewardDesc(), "Valid upto: " + rewards.getValidUpTo().concat(" | Newly added: ".concat(rewards.isSeen()?"NO":"YES")));
            cardArrayAdapter.add(card);
            rewards.setSeen(true);
        }
        listView.setAdapter(cardArrayAdapter);
    }
}