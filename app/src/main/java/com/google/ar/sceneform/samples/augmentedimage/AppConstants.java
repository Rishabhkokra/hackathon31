package com.google.ar.sceneform.samples.augmentedimage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppConstants {

    public static Integer score=0;

    public static final Map<Integer,Rewards> rewardsStore=new HashMap<>();

    public static final Map<Integer, List<Rewards>> userRewardsStore=new HashMap<>();
}
