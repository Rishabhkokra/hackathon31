package com.google.ar.sceneform.samples.augmentedimage;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ScoreActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.score_page);
        TextView txtEarned = (TextView) findViewById(R.id.scoreId);
        txtEarned.setText(String.valueOf(AppConstants.score));
    }


}